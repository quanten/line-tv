#include <Adafruit_NeoPixel.h>

#include <avr/power.h>

#define PIN            6


#define NUMPIXELS      21
#define NUMBYTES 63

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


byte buffer[NUMBYTES];

void setup() {
  pixels.begin();
  Serial.begin(115200);
}

void loop() {
  if (Serial.available() >= NUMBYTES) {
    Serial.readBytes(buffer, NUMBYTES);
    for (int i = 0; i < NUMPIXELS; i++) {
      pixels.setPixelColor(i, pixels.Color(buffer[i * 3] >> 0, buffer[i * 3 + 1] >> 0, buffer[i * 3 + 2] >> 0));
    }
    pixels.show();
    //delay(40);
  }
}
