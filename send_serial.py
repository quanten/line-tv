#!/bin/env python3

import serial
import time

ser = serial.Serial("/dev/ttyUSB0",115200)
file_bin = open("/tmp/testfifo","rb")

while True:
    ser.write(file_bin.read(63))
    ser.flush()
    time.sleep(0.03)
